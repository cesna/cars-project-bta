const express = require('express')
const app = express()
const port = process.env.PORT || 3000;
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const moment = require('moment');
moment.locale('lt');

const databaseUser = 'admin';
const databasePassword = 'testing123';
const databaseName = 'cars-project';
const databasePort = '47759';

const cars = require('./routes/cars.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

mongoose.connect(`mongodb://${databaseUser}:${databasePassword}@ds247759.mlab.com:${databasePort}/${databaseName}`, {useNewUrlParser: true});
app.get('/', (req, res) => res.send('Error 404'));
app.use('/cars', cars);

app.listen(port, () => console.log(`Server sukasi ant ${port} porto!`))