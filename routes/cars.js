const express = require('express');
const router = express.Router();
// var moment = require('moment');

const Car = require('../models/car');

router.get('/', (req, res) => {
    Car.find({}).exec((error, cars) => {
      if (error) {
        res.status(500).send(error.message);
      } else {
        console.log(cars);
        res.send(cars.map(cars => mapToCar(cars)));
      }
    });
  });

  router.post('/', (req, res) => {
    console.log('creating new car');
    const newCar = new Car();
  
    newCar.body_type = req.body.body_type;
    newCar.gearbox = req.body.gearbox;
    newCar.seats = req.body.seats;
    newCar.doors = req.body.doors;
    newCar.city = req.body.city;
    newCar.manufacturer = req.body.manufacturer;
    newCar.model = req.body.model;
    newCar.fuel = req.body.fuel;
    newCar.included = req.body.included;

    newCar.save((error, car) => {
      if (error) {
        res.status(500).send(error.message);
      } else {
        console.log(car);
        res.status(201).send(car);
      }
    });
  });

  router.get('/bmw', (req, res) => {
    Car.find({ manufacturer: "BMW" }).exec((error, cars) => {
      if (error) {
        res.status(500).send(error.message);
      } else {
        console.log(cars);
        res.send(cars);
      }
    });
  });

  // router.delete('/:id', (req, res) => {
  //   Car.findByIdAndRemove(
  //     {
  //       _id: req.params.id,
  //     },
  //     (error, success) => {
  //       if (error) {
  //         res.status(500).send(error.message);
  //       } else {
  //         console.log(success);
  //         res.status(200).send(success);
  //       }
  //     }
  //   );
  // });

  router.get('/:id', (req, res) => {
    Car.findOne({ _id: req.params.id }).exec((error, car) => {
      if (error) {
        res.status(500).send(error.message);
      } else {
        console.log(car);
        res.send(mapToCar(car));
      }
    });
  });

  const mapToCar = car => {
    return {
      id: car.id,
      gamintojas: car.manufacturer,
      modelis: car.model,
      kuro_tipas: car.fuel,
      kebulo_tipas: car.body_type,
      pavaru_deze: car.gearbox,
      sedymu_vietu_skaicius: car.seats,
      duru_skaicius: car.doors,
      papildomai: car.included,
      miestas: car.city
    }
  };

module.exports = router;