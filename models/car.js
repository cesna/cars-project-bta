const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CarSchema = new Schema({
  body_type: String,
  gearbox: String,
  seats: Number,
  doors: Number,
  city: String,
  manufacturer: String,
  model: String,
  fuel: String,
  included: String
 
});

module.exports = mongoose.model('Car', CarSchema);
